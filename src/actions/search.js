import {SET_ARRAIVAL_AIRPORT, SET_DEPARTURE_AIRPORT, SET_RETURN_DATE, SET_DEPARTURE_DATE, SET_ONE_WAY} from '../actions/types';

export const setArraivalAirport = (airport) => dispatch => {
    dispatch({
        type: SET_ARRAIVAL_AIRPORT,
        payload: airport
    })
}

export const setDepartureAirport = (airport) => dispatch => {
    dispatch({
        type: SET_DEPARTURE_AIRPORT,
        payload: airport
    })
}


export const setReturnDate = (date) => dispatch => {
    dispatch({
        type: SET_RETURN_DATE,
        payload: date
    })
}

export const setDepartureDate = (date) => dispatch => {
    dispatch({
        type: SET_DEPARTURE_DATE,
        payload: date
    })
}

export const setOneWay = (status) => dispatch => {
    dispatch({
        type: SET_ONE_WAY,
        payload: status
    })
}