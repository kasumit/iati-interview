import  {FETCH_AIRPORTS, FILTER_AIRPORTS} from './types';
import {Axios} from './axios';


export const fetchAirports = () => dispatch => {
        return Axios.post("airports")
        .then(response => {
            if(response.status === 200){
                dispatch({
                    type: FETCH_AIRPORTS,
                    payload: response.data.result
                })
            }

        })
        .catch(response => {
            dispatch({
                type: FETCH_AIRPORTS,
                payload: []
            })
            return response;
        })
}



export const filterAirports = (searchParam) => (dispatch, getState) => {
    const airports = getState().airport.items;
    const filteredAirports = airports.filter(airport => {
        const name = airport.name.toLowerCase();
        const code = airport.code.toLowerCase();
        return name.startsWith(searchParam) || code.startsWith(searchParam);
    })
    dispatch({
        type: FILTER_AIRPORTS,
        payload: filteredAirports
    });
}   