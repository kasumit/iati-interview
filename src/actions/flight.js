import  {FETCH_FLIGHTS, SORT_FLIGHTS} from './types';
import {Axios} from './axios';

export const fetchFlights = (flight) => dispatch => {
    return Axios.post("flight-search", flight)
    //return new Promise(resolve => setTimeout(() => resolve({data: data, status: 200}), 2000))
    .then(response => {
        if(response.status === 200){
            dispatch({
                type: FETCH_FLIGHTS,
                payload: response.data
            })
        }
        return response;
    })
    .catch(response => {
        dispatch({
            type: FETCH_FLIGHTS,
            payload: []
        })
        throw response.error.detail;
    })
}



export const sortByLowToHighPrice = () => (dispatch, getState) => {
    const flights = getState().flight.items;
    
    var sortedFlights = flights.sort((flight1, flight2) => {
        return flight1.fare.totalSingleAdultFare - flight2.fare.totalSingleAdultFare;
    });

    sortedFlights = sortedFlights.slice()

    dispatch({
        type: SORT_FLIGHTS,
        payload: sortedFlights
    });
}   

export const sortByHighToLowPrice = () => (dispatch, getState) => {
    const flights = getState().flight.items;
    
    var sortedFlights = flights.sort((flight1, flight2) => {
        return flight1.fare.totalSingleAdultFare - flight2.fare.totalSingleAdultFare;
    });

    sortedFlights.reverse();
    sortedFlights = sortedFlights.slice()

    dispatch({
        type: SORT_FLIGHTS,
        payload: sortedFlights
    });
}   




export const sortByEarlyToLate = () => (dispatch, getState) => {
    const flights = getState().flight.items;
    
    var sortedFlights = flights.sort((flight1, flight2) => {
        const date1 = flight1.legs[0].departureTime;
        const date2 = flight2.legs[0].departureTime;
        return new Date(date1) - new Date(date2);
    });

    sortedFlights = sortedFlights.slice()

    dispatch({
        type: SORT_FLIGHTS,
        payload: sortedFlights
    });
}

export const sortByLateToEarly = () => (dispatch, getState) => {
    const flights = getState().flight.items;
    
    var sortedFlights = flights.sort((flight1, flight2) => {
        const date1 = flight1.legs[0].departureTime;
        const date2 = flight2.legs[0].departureTime;
        return new Date(date1) - new Date(date2);
    });
    sortedFlights.reverse();
    sortedFlights = sortedFlights.slice()

    dispatch({
        type: SORT_FLIGHTS,
        payload: sortedFlights
    });
}