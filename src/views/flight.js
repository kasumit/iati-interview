import React, {Component} from 'react';
import {connect} from 'react-redux'
import {withRouter} from 'react-router';
import FlightTable from '../components/flight/table'
import SearchForm from '../components/search/SearchForm'
import FilterFlights from '../components/flight/filter'
import {Loader} from '../components/loader'
import {fetchFlights} from '../actions/flight'
import {setReturnDate, setOneWay, setDepartureDate, setDepartureAirport, setArraivalAirport} from '../actions/search'
import {fetchAirports} from '../actions/airport';


class Flight extends Component{

    state = {
        flightExist: true
    }


    componentDidMount(){
        if (window.performance) {
            if (performance.navigation.type === 1) {
                this.props.fetchAirports();
                this.fetchFlights();
                
            } 
          }
        //console.log(this.props.match.params.airport)
    }


    fetchFlights(){
        window.$('#loader').modal('toggle');

        var airports = this.props.match.params.airport;
        airports = airports.split("-")
        const from = airports[0];
        const to = airports[1];
        
        const departureDate = this.props.match.params.departure;
        const returnDate = this.props.match.params.return;
        
        this.props.setDepartureAirport(from)
        this.props.setArraivalAirport(to);
        this.props.setDepartureDate(departureDate);
        this.props.setReturnDate(returnDate);
        
        const flight = {
            fromAirport: from,
            toAirport: to,
            fromDate: departureDate,
            adult: 1
        }
        if(returnDate !== undefined){
            flight['returnDate'] = returnDate;
        }
      

        this.props.fetchFlights(flight)
        .then(response => {
            window.$('#loader').modal('toggle');
            this.setState({
                flightExist: true
            });

        })
        .catch(response => {
            window.$('#loader').modal('toggle');
            this.setState({
                flightExist: false,
            })
            alert(response)
        });
    }

    render(){
        return(
            <div className="container">
                <div className="row searc-form-in-flights">
                    <div className="col-md-12">
                        <SearchForm/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3">
                        <FilterFlights />
                    </div>
                    <div className="col-md-9">
                        <FlightTable/>
                    </div>
                </div>
                <Loader flightExist={this.state.flightExist}/>                 
            </div>           
        )
    }

}

export default withRouter(connect(null,
     {
         fetchFlights,
         setArraivalAirport,
         setDepartureAirport,
         setDepartureDate,
         setReturnDate,
         setOneWay,
         fetchAirports
        })(Flight));