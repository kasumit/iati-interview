import React, {Component} from 'react';
import SearchForm from '../components/search/SearchForm';
import {connect} from 'react-redux';
import {fetchAirports} from '../actions/airport';

class Search extends Component{

    componentWillMount(){
        this.props.fetchAirports();
    }

    render(){
        return(
            <div className="search-form" >
                <SearchForm />
            </div>
        )
    }

}

export default connect(null, {fetchAirports})(Search)