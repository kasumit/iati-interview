import React, { Component } from 'react';
import { Provider } from 'react-redux';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Search from './views/search'
import Flight from './views/flight'

import store from './store';

class App extends Component {




  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
              <Route exact path='/' component={Search} />
              <Route path='/flight-search/:airport/:departure/:return/:person' component={Flight} />
              <Route path='/flight-search/:airport/:departure/:person' component={Flight} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
