import {FETCH_FLIGHTS, SORT_FLIGHTS} from '../actions/types';

const initialState = {
    items: [],
    //items: data.result.flights,
    item: {},
}


export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_FLIGHTS:
            return {
                ...state,
                items: action.payload
            }
        case SORT_FLIGHTS:
            return {
                ...state,
                items: action.payload
            }
        default:
            return state;
    }
}