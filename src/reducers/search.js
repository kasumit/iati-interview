import {SET_ARRAIVAL_AIRPORT, SET_DEPARTURE_AIRPORT, SET_RETURN_DATE, SET_DEPARTURE_DATE, SET_ONE_WAY} from '../actions/types';
import * as moment from 'moment';


const initialState = {
    isOneWay: false,
    from: '',
    to: '',
    departureDate: moment().format("YYYY-MM-DD"),
    returnDate: '',

}


export default (state = initialState, action) => {
    switch(action.type){
        case SET_ARRAIVAL_AIRPORT:
            return {
                ...state,
                to: action.payload

            }
        case SET_DEPARTURE_AIRPORT:
            return {
                ...state,
                from: action.payload
            }
        case SET_RETURN_DATE:
            return {
                ...state,
                returnDate: action.payload
            }
        case SET_DEPARTURE_DATE:
            return {
                ...state,
                departureDate: action.payload
            }
        case SET_ONE_WAY:
            return {
                ...state,
                isOneWay: action.payload
            }                    
        default:
            return state;
    }
}