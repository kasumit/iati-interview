import {FETCH_AIRPORTS, FILTER_AIRPORTS} from '../actions/types';

const initialState = {
    items: [],
    filteredItems: []

}


export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_AIRPORTS:
            return {
                ...state,
                items: action.payload
            }
        case FILTER_AIRPORTS:
            return {
                ...state,
                filteredItems: action.payload
            }
        default:
            return state;
    }
}