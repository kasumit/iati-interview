import {combineReducers} from 'redux';

import airport from './airport';
import flight from './flight';
import search from './search';


export default combineReducers({
    airport: airport,
    flight: flight,
    search: search,
});
