import React from 'react';
import {connect} from 'react-redux';
import {filterAirports} from '../../actions/airport';

class SearchInput extends React.Component{

    constructor(){
        super();
         this.onChange = this.onChange.bind(this);

    }


    onChange(event){
        const value = event.target.value.toLowerCase();

        if(value.length > 2){
            this.props.filterAirports(value)
        }

        this.props.setLocation(event.target.value)
    }

    render(){
        const airports = this.props.airports.map((airport, index) => {
            return <option value={airport.code} key={index}>{airport.name}</option>
        })
        return(
            <div>
                <input 
                    className="search-input form-control" 
                    list="airports" 
                    name={this.props.placeholder} 
                    placeholder={this.props.placeholder} 
                    onChange={this.onChange}
                    defaultValue={this.props.defaultValue} />
                <datalist id="airports" key={this.props.placeholder}>
                    {airports}
                </datalist>                
            </div>
        )
    }

}



const mapStateToProps = state => ({
    airports: state.airport.filteredItems
})


export default connect(mapStateToProps, {filterAirports})(SearchInput)