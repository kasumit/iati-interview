import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router'
import {withRouter} from 'react-router';

import './DateInput';

import * as moment from 'moment';
import { DateInput } from './DateInput';
import  SearchInput from './SearchInput';
import {fetchFlights} from '../../actions/flight'
import {Loader} from '../loader'

import {setReturnDate, setOneWay, setDepartureDate, setDepartureAirport, setArraivalAirport} from '../../actions/search'

class SearchForm extends Component{
    


    state = {
        currentDate: moment().format("YYYY-MM-DD"),
        airports: [],
        loader: false,
        flightExist: false,        
    }

    constructor() {
        super();
        this.setDirection = this.setDirection.bind(this);
        this.onSearchBtnClick = this.onSearchBtnClick.bind(this);
    }

    componentDidMount(){
        if (window.performance) {
            if (performance.navigation.type === 1) {
                if(this.props.match.params.return === undefined){
                    this.props.setOneWay(true);
                    document.getElementById("one-way").checked = true;
                    document.getElementById("return").checked = false;

                }
            }
        }

    }

    setDirection(changeEvent){
        let isReturn = changeEvent.target.value === 'return';
        this.props.setOneWay(!isReturn)
        if(!isReturn){
            this.props.setReturnDate("")
            this.child.resetDate();

        }
    }
    
    onSearchBtnClick(event){

        if(this.props.from === '' || this.props.from.length < 3){
            alert("Departure Airport cannot be empty");
            return;
        }
        else if(this.props.to === '' || this.props.to.length < 3){
            alert("Landing Airport cannot be empty");
            return;
        }
        else if(this.props.departureDate === ''){
            alert("Departure Date cannot be empty");
            return;
        }
        else if(!this.props.isOneWay && this.props.returnDate === ''){
            alert("Return Date cannot be empty");
            return;
        }
        else{
            window.$('#loader').modal('toggle');
            const flight = {
                fromAirport: this.props.from,
                toAirport: this.props.to,
                fromDate: this.props.departureDate,
                adult: 1
            }
            if(!this.state.isOneWay){
                flight['returnDate'] = this.props.returnDate;
            }

            this.props.fetchFlights(flight)
            .then(response => {
                window.$('#loader').modal('toggle');
                this.setState({
                    flightExist: true
                });
            })
            .catch(response => {
                this.setState({
                    flightExist: false,
                })
                window.$('#loader').modal('toggle');
            });
        }
    }

    render(){
        
        if(this.state.flightExist){
            const redirect = `/flight-search/${this.props.from}-${this.props.to}/${this.props.departureDate}/1-0-0`;
            return <Redirect to={redirect}/>
        }

        return(
            <div>
                <div className="center">
                    <SearchInput 
                        name="fromAirport" 
                        placeholder="from" 
                        setLocation={this.props.setDepartureAirport} 
                        defaultValue={this.props.from}/>
                    <SearchInput 
                        name="toAirport" 
                        placeholder="to" 
                        setLocation={this.props.setArraivalAirport}
                        defaultValue={this.props.to}/>
                    <DateInput  
                        placeholder="Departure Date"  
                        min={this.state.currentDate}   
                        defaultValue={this.props.departureDate} 
                        setDate={this.props.setDepartureDate}/>
                    <DateInput
                        ref={instance => { this.child = instance; }}
                        placeholder="Return Date"     
                        min={this.state.departureDate} 
                        defaultValue={this.props.returnDate}  
                        visible={this.props.isOneWay} 
                        setDate={this.props.setReturnDate}/>      
                    
                    <button 
                    className="search-btn btn btn-dark" 
                    onClick={this.onSearchBtnClick}>
                        Search
                    </button>
                </div>                
                <div className="center flight-type">
                    <input  type="radio" value="return" name="flight-type" id="return" defaultChecked={true} onChange={this.setDirection} />
                    <label className="label">
                        Return
                    </label>
                    <input type="radio" value="one-way" name="flight-type" id="one-way"  onChange={this.setDirection}/>
                    <label className="label">
                        One-Way
                    </label>
                </div>
                <Loader flightExist={this.state.flightExist}/>                 
            </div>

        )
    }

}

const mapStateToProps = state => ({
    isOneWay: state.search.isOneWay,
    from: state.search.from,
    to: state.search.to,
    departureDate: state.search.departureDate,
    returnDate: state.search.returnDate,
})


export default withRouter(connect(mapStateToProps, 
    {fetchFlights,
    setArraivalAirport,
    setDepartureAirport,
    setDepartureDate,
    setReturnDate,
    setOneWay})(SearchForm));