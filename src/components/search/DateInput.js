import React from 'react';

export class DateInput extends React.Component{


    constructor(){
        super();
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {
            type: 'text',
            text: ''
        }
    }

    componentDidMount(){
        this.setState({
            text: this.props.defaultValue
        })
    }

    

    onFocus(){
        this.setState({
          type: 'date'
        });
    }
    onBlur(){
        this.setState({
          type: 'text'
        });
    }

    onChange(event){
        this.setState({
            text: event.target.value
        })
        this.props.setDate(event.target.value);
    }

    resetDate(){
        this.setState({
            text: ''
        })
    }


    render(){
        return(
            <input 
                className="search-input form-control date" 
                type={ this.state.type } 
                onFocus={ this.onFocus } 
                onBlur={ this.onBlur } 
                placeholder={this.props.placeholder} 
                min={this.props.min}
                disabled={this.props.visible}
                onChange={this.onChange}
                value={this.state.text}
                /> 
        )
    }

}