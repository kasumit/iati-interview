import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as moment from 'moment';

class FlightTable extends Component{

    timeFormat(date){
      return moment(date, "YYYY-MM-DD HH:mm:ss").format("HH:mm")
    }

    getTimeFromMins(mins) {
      var h = mins / 60 | 0,
          m = mins % 60 | 0;
      return h + "h " + m + " min ";
    }

    getTableRow(flight, index){

      const fare = flight.fare;

      var baggage = '15K'
      if("baggageAllowances" in fare){
         baggage = fare.baggageAllowances[0].amount + "" + fare.baggageAllowances[0].alternativeType;
      }

      var stopText = '';
      if(flight.legs.length > 1){
        stopText = (flight.legs.length -1) + ' stop'
      }
      var flighHours = 0;
      flight.legs.map((leg, index) => {
        flighHours += leg.legDurationMinute;
        flighHours += leg.waitTimeBeforeNextLeg;
        return leg;
      })
      
      return (
          <tr key={"flight-" + index}>
            <td>
              <div>{flight.legs[0].carrierName}</div>
              <div>{flight.legs[0].departureAirport} > {flight.legs[flight.legs.length - 1].arrivalAirport}</div>
            </td>
            <td>
              <div>
                {this.timeFormat(flight.legs[0].localDepartureDate)}
              </div>
              <div>
                  {this.getTimeFromMins(flighHours)} 
                  ({this.timeFormat(flight.legs[flight.legs.length - 1].localArrivalDate)})
              </div>                  
            </td>
            <td>
              <div>{baggage}</div>
              <div>{fare.type}</div>
              <div>{stopText}</div>
            </td>
            <td className="text-md-right">
              <div>
                <a data-toggle="collapse" href={"#detail-" + index} role="button" aria-expanded="false" aria-controls={"#detail-" + index}>
                <i className="fas fa-search"></i>
                </a>              
                <a className="btn btn-warning btn-price">
                  <span className="btn-price-text">
                  {fare.totalSingleAdultFare} {fare.currency}
                  </span>
                </a>
              </div>
            </td>        
          </tr>
      )
    }

    getTableRowDetails(flight, index){

      var baggage = '15K'
      const fare = flight.fares[0];
      if("baggageAllowances" in fare){
        baggage = fare.baggageAllowances[0].amount + "" + fare.baggageAllowances[0].alternativeType;
      }
    
    const flightDetails = flight.legs.map((leg, index ) => {

        const waitingTime = leg.waitTimeBeforeNextLeg !== 0 ?
          <div className="text-center gray-text">
              Waiting Duration |&nbsp;	
              {this.getTimeFromMins(leg.waitTimeBeforeNextLeg)}
          </div> 
          : 
          <div></div>;


        return(
          <div key={index}>
            <div className="card card-body" >
              <div className="row">
                  <div className="col-md-4">
                    <div>{leg.carrierName} {flight.flightNo}</div>
                    <div className="text-md-right">
                      {this.getTimeFromMins(leg.legDurationMinute)}
                    </div>
                    <div className="text-md-right">{baggage}</div>
                    <div className="gray-text">Free Seat {fare.type} {fare.freeSeatCount}</div>
                  </div>
                  <div className="col-md-7 offset-md-1">
                    <div className="gray-text">Airports</div>
                    <div className="airport-div">
                      <i className="fas fa-plane-departure fligt-icon"></i> 
                      {leg.departureAirportName}
                    </div>
                    <div className="airport-div">
                      <i className="fas fa-plane-arrival fligt-icon"></i>
                      {leg.arrivalAirportName}
                    </div>
                  </div>
              </div>
          </div>
          {waitingTime}
      </div>
        )
    })


      
      return (
          <tr key={"flight-detail-" + index} className="collapse" id={"detail-" + index}>
            <td colSpan="4">
              {flightDetails}
            </td>
          </tr>
      )
    }


    render(){
        var allFlights = [];
        this.props.flights.map((flight, index) => {
            var f = this.getTableRow(flight, index);
            var x = this.getTableRowDetails(flight, index);
            allFlights.push(f);
            allFlights.push(x);
            return flight;
        })
        return(
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">airline</th>
                  <th scope="col">take off</th>
                  <th scope="col">flight detail</th>
                  <th scope="col" className="text-md-right">price</th>
                </tr>
              </thead>
              <tbody>
                {allFlights}
              </tbody>
            </table>            
        )
    }
}


const mapStateToProps = state => ({
  flights: state.flight.items
})


export default connect(mapStateToProps)(FlightTable);