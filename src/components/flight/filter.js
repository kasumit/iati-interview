import React, {Component} from 'react';
import {connect} from 'react-redux'
import {sortByLowToHighPrice, sortByHighToLowPrice, sortByEarlyToLate, sortByLateToEarly} from '../../actions/flight';

class FilterFlights extends Component{
    render(){
        return(
            <div>
                <div className="flight-filter">
                    <a  href="#" onClick={this.props.sortByHighToLowPrice}>Price (high to low)</a>
                </div>
                <div className="flight-filter">
                    <a  href="#" onClick={this.props.sortByLowToHighPrice}>Price (low to high)</a>
                </div>
                <div className="flight-filter">
                    <a  href="#" onClick={this.props.sortByEarlyToLate}>Time (early to late)</a>
                </div>
                <div className="flight-filter">
                    <a  href="#" onClick={this.props.sortByLateToEarly}>Time (late to early)</a>
                </div>
            </div>

        )
    }
}


export default connect(null,
     {
        sortByHighToLowPrice,
        sortByLowToHighPrice,
        sortByEarlyToLate,
        sortByLateToEarly
    })(FilterFlights)